function createLocationOptions(locationsList) {
    let optionHTML =`<option selected value="">Choose a location</option>`;
    for (const location of locationsList) {
        optionHTML += `<option value="${location.id}">${location.name}</option>`
    }
    console.log(optionHTML);
    return optionHTML;
}


"We need to add an event listener for when the DOM loads."
window.addEventListener('DOMContentLoaded', async () => {
    // console.log("Event listener");

    "Let's declare a variable that will hold the URL for the API that we just created."
    const url = 'http://localhost:8000/api/locations/';
    // console.log(url);

    "Let's fetch the URL. Don't forget the await keyword so that we get the response, not the Promise."
    try {
        const response = await fetch(url);
        // console.log(response);
        "If the response is okay, then let's get the data using the .json method. Don't forget to await that, too."
        if (!response.ok) {
            console.log("error");
        } else {
            const data = await response.json();
            console.log(data);

            // You quickly confer and agree that the code needed when the page loads will call
            // that RESTful API you just created, get the data back, then loop through it. And
            // for each state in it, it'll create an option element that has a value of the
            // abbreviation and the text of the name.
            const html = createLocationOptions(data.locations)

            // ?? why does hint code use "get element by ID" instead of querySelector?
            const locationSelectTag = document.querySelector('#location');
            locationSelectTag.innerHTML = html;

            // Alternative from hint psuedocode:
            // const selectTag = document.getElementById('state');
            // for (let state of data.states) {
                // Create an 'option' element

                // Set the '.value' property of the option element to the
                // state's abbreviation

                // Set the '.innerHTML' property of the option element to
                // the state's name

                // Append the option element as a child of the select tag
        }

    } catch (e) {
        console.error(e);
    }

    // Listener for form submit event
    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log(json);
        // console.log('need to submit the form data');

        // Send data back to the server
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newConference = await response.json();
            console.log(newConference);
        }
    });


});
