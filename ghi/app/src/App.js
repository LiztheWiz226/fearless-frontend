import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <>
      <Nav />
      <div className="container">
        {/* <AttendeesList attendees={props.attendees} /> */}
        <LocationForm />
      </div>
    </>
  );
}

export default App;
